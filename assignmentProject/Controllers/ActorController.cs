﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using assignmentProject.Models;
using assignmentProject.Models.ViewModels;
using System.Diagnostics;


namespace assignmentProject.Controllers
{    /*reference from class example*/
    public class ActorController : Controller
    {
        private MovieCMSContext db = new MovieCMSContext();

        // GET: Actor
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {

            IEnumerable<Actor> actors = db.Actors.ToList();
            return View(actors);
        }
        //get : create actor
        public ActionResult New()
        {


            ActorEdit actoreditview = new ActorEdit();
            return View(actoreditview);
        }


        [HttpPost]
        public ActionResult New(string ActorFName_New, string ActorLName_New)
        {
            // Basic query   
            string query = "insert into actors (ActorFName, ActorLName) values (@FirstName, @LastName)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@FirstName", ActorFName_New);
            myparams[1] = new SqlParameter("@LastName", ActorLName_New);


            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("List");
        }



        public ActionResult Edit(int id)
        {
            //For edit we need a list of actors 


            ActorEdit actoreditview = new ActorEdit();

            actoreditview.actor = db.Actors.Find(id);

            return View(actoreditview);
        }
        [HttpPost]
        public ActionResult Edit(int id, string ActorFName, string ActorLName)
        {
            //If the ID doesn't exist or the actor doesn't exist
            if ((id == null) || (db.Actors.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "update actors set ActorFName=@FirstName, ActorLName=@LastName where actorid=@id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@FirstName", ActorFName);
            myparams[1] = new SqlParameter("@LastName", ActorLName);
            myparams[2] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);
            return RedirectToAction("Show/" + id);
        }
        public ActionResult Show(int? id)
        {
            //If the id doesn't exist or the actor doesn't exist
            if ((id == null) || (db.Actors.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from actors where actorid=@id";

            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);


            Actor myactor = db.Actors.SqlQuery(query, myparams).FirstOrDefault();
            return View(myactor);
        }
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Actors.Find(id) == null))
            {
                return HttpNotFound();

            }
            // delete associated movies
            string query = "delete from Movies where actor_ActorID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            // delete actor
            query = "delete from Actors where actorid=@id";
            param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }



    }
}