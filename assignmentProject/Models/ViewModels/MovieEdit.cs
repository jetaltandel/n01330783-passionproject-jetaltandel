﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignmentProject.Models.ViewModels
{
    public class MovieEdit
    {
        public MovieEdit()
        {

        }
        public virtual Movie movie { get; set; }
        public IEnumerable<Actor> actors { get; set; }
    }
}