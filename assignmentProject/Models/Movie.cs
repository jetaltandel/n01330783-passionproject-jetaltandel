﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace assignmentProject.Models
{
    public class Movie
    {

        // Movie property
        [Key]
        public int MovieID { get; set; }

        [Required, StringLength(250), Display(Name = "Name")]
        public string MovieName { get; set; }
        //movie has actor
        public virtual Actor actor { get; set; }
    }
}