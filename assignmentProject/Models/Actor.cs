﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace assignmentProject.Models
{
    public class Actor
    {

        //actor property
        [Key,ScaffoldColumn(false)]
        public int ActorID { get; set; }

        [Required, StringLength(150), Display(Name = "FirstName")]
        public string ActorFName { get; set; }

        [Required, StringLength(150), Display(Name = "LastName")]
        public string ActorLName { get; set; }
        // one actor have many movies
        public virtual ICollection<Movie> Movies { get; set; }
    }
}